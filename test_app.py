import os
import pytest
from app import app

@pytest.fixture
def client():
    app.config['TESTING'] = True
    client = app.test_client()

    yield client

def test_index(client):
    """Start with a blank database."""

    rv = client.get('/')
    assert b'Index' in rv.data

def test_hello_name(client):
    names =['budi', 'agus', 'iwan', 'susi', 'rudi']
    for name in names:
        rv = client.get('/hello/{}'.format(name))
        expected = bytearray('Hello {}'.format(name), 'utf-8')
        assert expected in rv.data
